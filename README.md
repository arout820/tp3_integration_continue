# TP3 INTEGRATION CONTINUE

## Dossier du projet

La racine de mon projet contient les éléments suivants:

- README.md
- **.gitlab-ci.yml**
- **script1_deploy.sh**
- **script2_register_runner.sh**
- app (dossier)
- gitlab (dossier crée lors du script1)
- docker_compose.yml
- dockerfile
- requirements.txt

## Prérequis

- 1 ordi : ordi portable ou ordi fixe
- 1 ligne de commande linux : soit vm ubuntu soit wsl sur windows
- Docker-desktop sur votre machine
- Rajout de certaines variables à la main sur la plateforme gitlab-ce local

## Pipeline: .gitlab-ci.yml

La pipeline sur gitlab-ci prend toujours ce nom.

En haut de la pipleine j'ai rajouté une image : python:3.9 afin de pouvoir exécuter les tests dans des evironnements python (rechangé à l'étape du build et push).

J'ai également rajouté le tag que j'utiliserai pour le runner. Le tag est à modifier si vous avez modifié le nom du tag par défaut **default_tag_tp3** lors de l'execution du premier script.
 
Les tests à effectuer sont : 

- le lint pour vérifier les normes de codage
- l'analyse de la complexité cyclomatique
- les tests unitaires
- le build via un dockerfile fourni dans la racine du projet
- le push sur le docker hub

Chaque étape de tests permet de générer des artifacts, qui permettra de stocker les tests et également de prouver le bon fonctionnement des tests.

**IMPORTANT :**

A l'étape push sur le répo docker hub, il y a des informations sensibles qui sont stockés en forme de variables protégés. Ne les mettez pas à la main dans le code, il faut soit passer par des vaults, soit mettre directement à la main dans l'espace dédié avec les noms variables indiqués en bas.

**TOKEN_DOCKER_HUB**: récupérable dans l'espace sécurité de votre compte docker hub  

**USER_DOCKER_HUB**: nom de compte docker hub

Il faut rajouter ces deux variables dans l'espace **"Variables"** sur l'url http://localhost/admin/application_settings/ci_cd


## Premier script : script1_deploy.sh

Ce script permet la création des environnements locaux (containers docker) via le docker-compose:
- gitlab-ce
- gitlab-runner

### ETAPE 1/2 - CREATION DOSSIER ET DOCKER COMPOSE
L'étape permet donc de créer un dossier gitlab à la racine du projet, de copier le docker-compose à l'intérieur puis de l'éxecuter afin de créer les environnements.

L'étape permet également de pouvoir avoir accès au mot de passe crée pour la prem_re connexion sur le gitlab-ce avec l'utilisateur root.

### ETAPE 2/2 - TUTO POUR CONFIGURER GITLAB


Maintenant que les containers ont été crées, il faut configurer le gitlab local, allez donc sur l'url http://localhost:80 et effectuer les actions plus bas, lisez attentivement:

- Il faudra tout d'abord attendre un petit peu que le container se mette en place (3 minutes environ), puis se connecter avec l'username root et le mot de passe donné plus haut, avec la commande.

- Ensuite dans gitlab-ce il faut décocher "Sign-up enabled" pour la création de nouveaux comptes : http://localhost/admin/application_settings/general#js-signup-settings

- Changer le nom d'utilisateur : http://localhost/-/profile/account

- Changer le mot de passe : http://localhost/-/profile/password/edit

- Puis il faudra vous reconnecter avec les nouveaux idientifiants.

- Vous devez également dans les paramètres d'import de votre compte admin **Import and exports settings**, cochez la case pour "**url respository**", cela vous permettra de pouvoir fetch votre projet dans votre container gitlab local : http://localhost/admin/application_settings/general

- Maintenant vous pouvez lancer **le deuxième script** qui permettra la création du runner, oubliez pas de le lancer en **sudo** sinon la partie de configuration du **config.toml** ne fonctionnera pas.

## Deuxième script : script2_register_runner.sh

Ce script permet la création du runner. Le script peut attendre un paramètre qui indiquera le nom de tag du runner a créer, le tag par defaut est **default_tag_tp3**. Pour les besoin de l'exercice pas besoin de changer le nom du tag. 

Il faudra lancer le script en **sudo** afin de permettre à la dernière étape de configuration du config.yoml de fonctionner.

### ETAPE 1/4 - VÉRIFICATION TAG

On vérifie si un paramtre a été spécifié au lancement du script, si non on reste sur le tag par défaut. Puis on vérifie si un fichier config.toml existe déjà. On vérifie également si un runner existe déjà avec le nom donné. Si un runner existe déjà avec le nom, on arrête le script sinon on passe à l'étape 2.

### ETAPE 2/4 - SAISIE DU TOKEN PAR L'UTILISATEUR

L'étape nous demande de saisir le registration token pour la création de runner. Le token se trouve à l'adresse: http://localhost/admin/runners

Vous pouvez le retrouver en cliquant sur les 3 points à droite de **"New instance runner"**

### ETAPE 3/4 - CREATION RUNNER DANS CONTAINER GITLAB-RUNNER

L'étape permet la création du runner en mode non interactive.

### ETAPE 4/4 - RAJOUTS ELEMENTS DANS CONFIG.TOML

Cette étape permet de rajouter le network mode à la fin du fichier **config.toml**.

L'étape reste a amélrioer afin de pouvoir prendre en compte toutes les possibilités et de rajouter l'élément en question d'une meilleur façon.

## Dossier app

Dans ce dossier se trouve l'application sur lequel on va effectuer les tests dans la pipeline puis déployer à la fin sur le docker hub.

## Dossier gitlab

Ce dossier est crée via le premier script. Ici se trouve la configuration du gitlab-runner et du gitlab-ce.


## Arborescense


```bash
.
├── Dockerfile
├── README.md
├── app
│   ├── application
│   │   ├── __init__.py
│   │   └── application.py
│   ├── command
│   │   ├── __init__.py
│   │   └── command.py
│   ├── machine
│   │   ├── __init__.py
│   │   ├── machine.py
│   │   └── machines.json
│   ├── main.py
│   ├── requirements.txt
│   └── test
│       ├── __init__.py
│       ├── system
│       │   └── machine.robot
│       └── unit
│           ├── __init__.py
│           └── test.py
├── docker-compose.yml
├── push.sh
├── requirements.txt
├── script1_deploy.sh
└── script2_register_runner.sh
```

Fin du readme, je vous remercie