#________________________________________________________________________________________#
# ------ ETAPE 1/4 - VÉRIFICATION TAG ------ #
echo -e "\033[1;35m- ETAPE 1/4 - VERIFICATION TAG\033[0m"

if [ "$1" ]; then
  RUNNER_TAG="$1"
  echo "Le tag séléctionné est : [$RUNNER_TAG] "
else
  RUNNER_TAG="default_tag_tp3"
  echo "Vous n'avez pas séléctionné de tag en arguments, le tag par défaut est [$RUNNER_TAG]"
fi

FILE_TEST=gitlab/gitlab-runner/config.toml

# Vérifier présence config.toml
if ! cat "$FILE_TEST" 2> /dev/null; then
  echo "Le fichier '$FILE_TEST' n'existe pas. Vous pouvez créer le runner [$RUNNER_TAG] demandé. Continuez les étapes."
elif cat "$FILE_TEST" | grep -q "$RUNNER_TAG"; then
  echo "Un runner avec le tag '$RUNNER_TAG' existe déjà. Le script s'arrête."
  exit 1
fi

#________________________________________________________________________________________#
# ------ ETAPE 2/4 - SAISIE DU TOKEN PAR L'UTILISATEUR ------ #
echo -e "\033[1;35m- ETAPE 2/4 - SAISIE DU TOKEN PAR L'UTILISATEUR\033[0m"

echo "Veuillez saisir la valeur de la variable REGISTRATION_TOKEN trouvable dans les paramètres CI-CD sur le gitlab:"
read REGISTRATION_TOKEN

# Vérifier si l'utilisateur n'a pas saisi de valeur
if [ -z "$REGISTRATION_TOKEN" ]; then
  echo "Vous n'avez pas saisi de valeur pour REGISTRATION_TOKEN. Le script s'arrête."
  exit 1
fi

export REGISTRATION_TOKEN="$REGISTRATION_TOKEN"

#________________________________________________________________________________________#
# ------ ETAPE 3/4 - CREATION RUNNER DANS CONTAINER GITLAB-RUNNER ------ #
echo -e "\033[1;35m- ETAPE 3/4 - CREATION RUNNER DANS CONTAINER GITLAB-RUNNER\033[0m"

FILE_TEST=gitlab/gitlab-runner/config.toml

if cat "$FILE_TEST" | grep -q "$RUNNER_TAG"; then
  echo "Le runner '$RUNNER_TAG' existe déjà. Aucune action nécessaire."
else
  echo "Le runner '$RUNNER_TAG' n'existe pas. Enregistrement en cours..."
  docker exec -it gitlab-runner gitlab-runner register \
    --non-interactive \
    --url "http://gitlabhost/" \
    --registration-token "$REGISTRATION_TOKEN" \
    --executor "docker" \
    --docker-image alpine:latest \
    --description "$RUNNER_TAG" \
    --tag-list "$RUNNER_TAG"
fi

# ________________________________________________________________________________________#
# ------ ETAPE 4/4 - RAJOUTS ELEMENTS DANS CONFIG.TOML ------ #
echo -e "\033[1;35m- ETAPE 4/4 - RAJOUTS ELEMENTS DANS CONFIG.TOML\033[0m"

export FILE_PATH="gitlab/gitlab-runner/config.toml"

# réseau utilisé par le container gitlab-ce 
NETWORK_NAME=$(docker inspect gitlab-ce | grep -A 1 Networks | awk 'NR==2 { gsub(/"/, ""); gsub(/:/, ""); gsub(/{/, ""); gsub(/^[ \t]+/, ""); gsub(/[ \t]+$/, ""); print $0 }' | tr -d '\n')

if [ -f "$FILE_PATH" ]; then
  # rajout network_mode fin fichier config.toml si non présent (4 espaces nécéssaire avant le rajout pour bonne indentation)
  if ! grep -q "network_mode = \"$NETWORK_NAME\"" "$FILE_PATH"; then
    echo "    network_mode = \"$NETWORK_NAME\"" >> "$FILE_PATH"
    echo "Ligne 'network_mode' ajoutée avec succès au fichier $FILE_PATH."
  else
    echo "La ligne 'network_mode' existe déjà dans le fichier $FILE_PATH. Aucune modification n'est nécessaire."
  fi

  # Recherche de la ligne volumes dans le fichier
  if grep -q 'volumes' $FILE_PATH; then
      # Modification de la ligne
      sed -i 's|volumes = .*|volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]|' $FILE_PATH
      echo "Ligne 'volume' modifiée avec succès au fichier $FILE_PATH."
  else
    echo "Les infos sont déjà présents." 
  fi

else
  echo "Le fichier $FILE_PATH n'a pas été trouvé. Assurez-vous de spécifier le bon chemin."
fi
